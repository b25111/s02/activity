try:
    year = int(input("Enter year: "))
except:
    year = int(input("Enter valid year: "))

if (year % 400 == 0) and (year % 100 == 0):
    print(f"{year} is a leap year")
elif (year % 4 == 0) and (year % 100 != 0):
    print(f"{year} is a leap year")
else:
    print(f"{year} is not a leap year")



columns = int(input(f"Enter number of columns\n"))
rows = int(input(f"Enter number of rows\n"))


for x in range(rows):
    for y in range(columns):
        print('☆',end = ' ')
    print()